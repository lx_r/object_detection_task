
#! python3
# -*- encoding: utf-8 -*-
'''
@File    :   ddp_train.py
@Time    :   2024/01/14 15:12:02
@Author  :   EthanLee
@Version :   1.0
@Contact :   lx9mechanic@gmail.com
@License :   MIT
@Desc    :  
# running cmd: CUDA_VISIBLE_DEVICES=3,4 torchrun -nnodes 1 --nproc_per_node 2 dpp_train.py

# [epoch: 31] step: 23, learning_rate: [0.0007911220577405485] average loss: 3.66: 
# 100%|██████████████████████████████████████████████| 5/5 [00:01<00:00,  3.01it/s]
# Val Accuracy: 0.239
# New Best Accuracy: 0.239, Model Saved: best.pt
# [epoch: 32] step: 23, learning_rate: [0.0007790686370876671] average loss: 3.635:
# 100%|██████████████████████████████████████████████| 5/5 [00:01<00:00,  3.18it/s]
# Val Accuracy: 0.247
# New Best Accuracy: 0.247, Model Saved: best.pt
'''


import torch
import torchvision
import os
import math
import tqdm
import sys

batch_size=256
epoches = 100
num_classes = 10

torch.distributed.init_process_group(backend='nccl')

transform = torchvision.transforms.Compose([
                torchvision.transforms.Resize(128),
                torchvision.transforms.ToTensor(),
                torchvision.transforms.Normalize(mean=[0.5, 0.5, 0.5],
                                                  std=[1.0, 1.0, 1.0])])
train_dataset = torchvision.datasets.CIFAR10(root="./data/cifar10",
                                             train=True,
                                             download=True,
                                             transform=transform)
val_dataset = torchvision.datasets.CIFAR10(root="./data/cifar10",
                                           train=False,
                                           download=True,
                                           transform=transform)
num_classes = len(val_dataset.classes)

train_sampler = torch.utils.data.distributed.DistributedSampler(train_dataset)
val_sampler = torch.utils.data.distributed.DistributedSampler(val_dataset)
train_batch_sampler = torch.utils.data.BatchSampler(train_sampler, 
                                                    batch_size, 
                                                    drop_last=True)
train_dataloader = torch.utils.data.DataLoader(train_dataset,
                                               batch_sampler=train_batch_sampler,
                                               pin_memory=True,
                                               num_workers=4)
val_dataloader = torch.utils.data.DataLoader(val_dataset,
                                             batch_size=batch_size,
                                             sampler=val_sampler,
                                             pin_memory=True,
                                             num_workers=4)

device = f'cuda:{os.getenv("LOCAL_RANK")}' if torch.cuda.is_available() else 'cpu'
device = torch.device(device)

m = torchvision.models.mobilenet_v3_small(pretrained=False, 
                                          num_classes=num_classes)
ckpt_path = "/tmp/init_weight.pt"
if int(os.getenv("LOCAL_RANK")) == 0:
    torch.save(m.state_dict(), ckpt_path)
torch.distributed.barrier()
m.load_state_dict(torch.load(ckpt_path, map_location=device))
m = torch.nn.SyncBatchNorm.convert_sync_batchnorm(m).to(device)
m = torch.nn.parallel.DistributedDataParallel(m, device_ids=[int(os.getenv("LOCAL_RANK"))])

params = [ param for param in m.parameters() if param.requires_grad ]
optimizer = torch.optim.SGD(params=params,
                            lr=0.001,
                            momentum=0.9,
                            weight_decay=0.005)
lr_func = lambda x : (1 + math.cos(x * math.pi / epoches)) / 2 * (1 - 0.1) + 0.1
scheduler = torch.optim.lr_scheduler.LambdaLR(optimizer=optimizer,
                                              lr_lambda=lr_func)
loss_func = torch.nn.CrossEntropyLoss()

for epoch in range(epoches):
    train_sampler.set_epoch(epoch)
    m.train()
    optimizer.zero_grad()
    avg_loss = torch.zeros(1, device=device)
    right_pred_num = torch.zeros(1, device=device)
    best_acc = 0.0
    if int(os.getenv("LOCAL_RANK")) == 0:
        pbar = tqdm.tqdm(train_dataloader, file=sys.stdout)
    else:
        pbar = train_dataloader
    for i, (image, label) in enumerate(pbar):
        image = image.to(device)
        label = label.to(device)
        pred = m(image)
        loss = loss_func(pred, label)
        loss.backward()
        torch.distributed.all_reduce(loss)
        avg_loss = (avg_loss * i + loss.detach()) / (i + 1)
        if int(os.getenv("LOCAL_RANK")) == 0:
            pbar.desc  = f"[epoch: {epoch}] step: {i}, learning_rate: {scheduler.get_last_lr()} average loss: {round(avg_loss.item(), 3)}"
        assert torch.isfinite(loss), f"Nan Loss, Training End."
        optimizer.step()
        optimizer.zero_grad()
    torch.cuda.synchronize(device=device)
    m.eval()
    with torch.no_grad():
        if int(os.getenv("LOCAL_RANK")) == 0:
            pbar = tqdm.tqdm(val_dataloader, file=sys.stdout)
        else:
            pbar = val_dataloader
        for i, (image, label) in enumerate(pbar):
            image = image.to(device)
            label = label.to(device)
            pred = m(image)            
            pred = torch.max(pred, dim=1)[1]
            right_pred_num += torch.eq(pred, label).sum()
        torch.cuda.synchronize(device=device)
        torch.distributed.all_reduce(right_pred_num)
        if int(os.getenv("LOCAL_RANK")) == 0:
            acc = round(right_pred_num.item() / len(val_dataset), 3)
            print(f"Val Accuracy: {acc}")
            if acc > best_acc:
                best_acc = acc
                print(f"New Best Accuracy: {acc}, Model Saved: best.pt")
                torch.save(m.state_dict(), "best.pt")
