
#!/media/lx/data/sw/anaconda3/envs/detectron/bin/python
import torch
import torchvision
import PIL
import cv2
import numpy as np
import json
from pathlib import Path
import random
from typing import List, Optional
from scipy.optimize import linear_sum_assignment
import math
import matplotlib.pyplot as plt
from torch.utils.tensorboard import SummaryWriter

def generate_mini_coco_detect_dataset(data_path=Path("./datasets/")):
    inst_color = (0, 255, 255)
    bg_colors = np.array([[  5, 125,  65],  
                          [ 76, 157, 101], 
                          [ 64, 148, 71],
                          [ 41, 153, 97],
                          [ 27, 163, 60],
                          [ 29, 157, 106]], dtype=np.uint8)
    bg_colors[:, [0, 2]] = bg_colors[:, [2, 0]] # RGB2BGR

    pw, ph = 60, 60
    inst_patch = np.zeros((ph, pw), dtype=np.uint8)
    cv2.ellipse(inst_patch, (pw // 2, ph // 2), (pw // 2 - 4, ph // 2 - 4), 0.,0, 315, 255, cv2.FILLED)
    
    w, h = (120, 120)
    image = np.zeros((h, w, 3), dtype=np.uint8)
    image_mask = np.zeros((h, w), dtype=np.uint8)
    margin = 50
    gxx, gyy = np.meshgrid(range(margin, w - margin), range(margin, h - margin))
    grids = np.concatenate((gxx[:, :, None], gyy[:, :, None]), axis=-1).reshape((-1, 2))
    number = 640
    cnt = 0
    trial_inst_times_thresh = 2
    (data_path / "images/train").mkdir(exist_ok=True, parents=True)
    (data_path / "images/val").mkdir(exist_ok=True, parents=True)
    (data_path / "annotations").mkdir(exist_ok=True, parents=True)
    categories = [dict(supercategory="monster",
                       id=0,
                       name="monster",
                       instance_count=0)]
    coco_annotation_image = dict()
    coco_images = []
    val_number = 4
    anno_id = 0
    while cnt < number:
        cnt += 1
        img_cur = image.copy()
        img_mask_cur = image_mask.copy()
        trial_times = 0
        boxes = []
        segments = []
        while trial_times < trial_inst_times_thresh:
            trial_times += 1
            rotate_code = random.choice([-100, cv2.ROTATE_180, cv2.ROTATE_90_CLOCKWISE, cv2.ROTATE_90_COUNTERCLOCKWISE])
            r = 1.2 - 0.4 * random.random()
            inst_cur = inst_patch
            if rotate_code != -100:
                inst_cur = cv2.rotate(inst_cur, rotate_code)
            inst_cur = cv2.resize(inst_cur, (int(pw * r), int(ph * r)))
            iph, ipw = inst_cur.shape
            hipw, hiph = iph // 2, ipw // 2
            cx, cy = random.choice(grids)
            x1, y1, x2, y2 = cx - hipw, cy - hiph, cx + ipw - hipw, cy + iph - hiph
            inst_img_mask = img_mask_cur[y1:y2, x1:x2]
            if np.any(inst_img_mask[inst_cur==255]):
                continue
            img_cur[y1:y2, x1:x2][inst_cur==255] = np.array(inst_color, dtype=np.uint8)
            img_mask_cur[y1:y2, x1:x2][inst_cur==255] = 1
        cts, _ = cv2.findContours(img_mask_cur, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        for ct in cts:
            x, y, tpw, tph = cv2.boundingRect(ct)
            boxes.append([x, y, tpw, tph]) # l t w h
            segments.append(ct)
        img_cur[img_mask_cur!=1] = random.choice(bg_colors)
        basename = cnt
        coco_image_obj = dict(file_name=f"{basename}.jpg",
                              id = cnt,
                              width = w, 
                              height = h)
        coco_images.append(coco_image_obj)
        coco_annotation_image[basename] = []
        for i, (box, segment) in enumerate(zip(boxes, segments)):
            
            coco_annotation_image[basename].append(dict(
                id=anno_id,
                image_id=cnt,
                area=cv2.contourArea(segment),
                category_id=0,
                segmentation=[ int(x) for x in list(segment.reshape(-1))],
                bbox=[int(x) for x in box]
            ))
            anno_id += 1
        if cnt < number - val_number + 1:
            cv2.imwrite(str(data_path / "images" / "train" /  f"{basename}.jpg"), img_cur)
        else:
            cv2.imwrite(str(data_path / "images" / "val" /  f"{basename}.jpg"), img_cur)

    train_images = coco_images[:-val_number]
    val_images = coco_images[-val_number:]
    train_annotations = []
    for image_obj in train_images:
        train_annotations.extend(coco_annotation_image[image_obj["id"]])
    val_annotations = []
    for image_obj in val_images:
        val_annotations.extend(coco_annotation_image[image_obj["id"]])
    with open(str(data_path / "annotations" / "train.json"), "w") as f, \
         open(str(data_path / "annotations" / "val.json"), "w") as f_val:
        json.dump(dict(images=train_images, annotations=train_annotations, categories=categories), f)
        json.dump(dict(images=val_images, annotations=val_annotations, categories=categories), f_val)    
    
class MiniCOCODetectDataset(torch.utils.data.Dataset):
    def __init__(self, 
                 mode, 
                 data_path=Path("./datasets/"),
                 transforms = None):
        if not (data_path / "annotations" / f"{mode}.json").exists():
            generate_mini_coco_detect_dataset(data_path)

        self._transforms = transforms
        if self._transforms is None:
            self._transforms = torchvision.transforms.Compose([torchvision.transforms.ToTensor()])
            
        self.mode = mode
        
        label_file = data_path / "annotations" / f"{mode}.json"
        self.data_path = data_path        
        self.annos = dict() # group by image, key is image id
        with open(label_file, "r") as f:
            labels = json.load(f)
            self.image_objs = labels["images"]
            for anno in labels["annotations"]:
                if anno["image_id"] not in self.annos:
                    self.annos[anno["image_id"]] = []
                self.annos[anno["image_id"]].append(anno)

    def __len__(self):
        return len(self.image_objs)

    def __getitem__(self, idx):
        image_obj = self.image_objs[idx]
        image_path = self.data_path / "images" / self.mode / image_obj["file_name"]
        image = PIL.Image.open(str(image_path))
        orig_size = image.size
        anno_obj = self.annos[image_obj["id"]]
        labels = [anno["category_id"] for anno in anno_obj]
        boxes = [anno["bbox"] for anno in anno_obj]
        image_id = image_obj["id"]
        areas = [anno["area"] for anno in anno_obj]
        iscrowd = [0] * len(boxes)
        size = orig_size
        ow, oh = size
        for i, box in enumerate(boxes):
            l, t, w, h = box
            boxes[i] = [(l + w/2) / ow, (t + h/2) / oh, w / ow, h / oh]
            
        target = dict(boxes=torch.Tensor(boxes),
                      labels=torch.as_tensor(labels, dtype=torch.long),
                      image_id=torch.Tensor([image_id]),
                      area=torch.Tensor(areas),
                      iscrowd=torch.Tensor(iscrowd),
                      orig_size=torch.Tensor(orig_size),
                      size=torch.Tensor(size))
        image = self._transforms(image)

        return image, target

def collate_fn(batch):
    batch = list(zip(*batch))
    batch[0] = [x.unsqueeze(0) for x in batch[0]]
    batch[0] = torch.concat(batch[0])
    return tuple(batch)

class PositionEmbeddingSine(torch.nn.Module):
    def __init__(self, 
                 num_pos_feats=64,
                 temperature=10000,
                 normalize=False,
                 scale=None
                 ):
        super(PositionEmbeddingSine, self).__init__()
        self.num_pos_feats = num_pos_feats
        self.temperature = temperature
        self.normalize = normalize
        if scale is not None and normalize is False:
            raise ValueError("normalize should be True if scale is passed")
        if scale is None:
            scale = 2 * math.pi
        self.scale = scale
        
    def forward(self,x):
        """_summary_

        Args:
            x (_type_): N, C, H, W

        Returns:
            position : N, d_model, H, W
        """
        N, C, H, W = x.shape
        y_embed = torch.ones(H, dtype=torch.float32, device=x.device)
        y_embed = y_embed.cumsum(0)
        y_embed = y_embed[None, :, None]
        y_embed = y_embed.repeat((N, 1, W))
        
        x_embed = torch.ones(W, dtype=torch.float32, device=x.device)
        x_embed = x_embed.cumsum(0)
        x_embed = x_embed[None, None, :]
        x_embed = x_embed.repeat((N, H, 1))
        if self.normalize:
            eps = 1e-6
            y_embed = y_embed / (y_embed[:, -1:, :] + eps) * self.scale
            x_embed = x_embed / (x_embed[:, :, -1:] + eps) * self.scale
        
        d_model = torch.arange(self.num_pos_feats, dtype=torch.float32, device=x.device)
        d_model = self.temperature ** (2 * (d_model // 2) / self.num_pos_feats)
        
        pos_x = x_embed[:, :, :, None] / d_model
        pos_y = y_embed[:, :, :, None] / d_model
        pos_x = torch.stack((pos_x[:, :, :, 0::2].sin(), pos_x[:, :, :, 1::2]), dim=4).flatten(3)
        pos_y = torch.stack((pos_y[:, :, :, 0::2].sin(), pos_y[:, :, :, 1::2]), dim=4).flatten(3)
        pos = torch.cat((pos_y, pos_x), dim=3).permute(0, 3, 1, 2)
        return pos


class MLP(torch.nn.Module):
    def __init__(self, input_dim, hidden_dim, output_dim, num_layers):
        super(MLP, self).__init__()
        self.num_layers = num_layers
        h = [hidden_dim] * (num_layers - 1)
        layers = [torch.nn.Linear(n, k) if k != output_dim else torch.nn.Linear(n, k) \
                    for n, k in zip([input_dim] + h, h + [output_dim])]
        act_layers = []
        for i,layer in enumerate(layers):
            if i == num_layers - 1:
                act_layers.append(layer)
            else:
                act_layers.extend([layer, torch.nn.ReLU()])
        self.layers = torch.nn.Sequential(*act_layers)
    
    def forward(self, x):
        return self.layers(x)


class DETR(torch.nn.Module):
    def __init__(self, d_model=64, 
                 num_classes=1,
                 num_queries=100
                 ) -> None:
        super(DETR, self).__init__()
        # self.backbone = torchvision.models.mobilenet_v3_small(pretrained=True)
        # del self.backbone.fc
        # del self.backbone.classifier
        # del self.backbone.avgpool
        
        self.backbone = torchvision.models.resnet18()
        del self.backbone.fc
        
        self.pos_embed = PositionEmbeddingSine(num_pos_feats=32)

        self.transformer = torch.nn.Transformer(d_model=d_model,
                                                nhead=4,
                                                num_encoder_layers=2,
                                                num_decoder_layers=2,
                                                dim_feedforward=128)
        
        self.class_embed = torch.nn.Linear(d_model, num_classes + 1)
        self.box_embed = MLP(d_model, d_model, 4, 3)
        self.query_embed = torch.nn.Embedding(num_queries, d_model)
        self.input_project = torch.nn.Conv2d(128, d_model, kernel_size=1)
        
    def forward(self, x):
        bs, c, h, w = x.shape
        x = self.backbone.conv1(x)
        x = self.backbone.bn1(x)
        x = self.backbone.relu(x)
        x = self.backbone.maxpool(x)

        x = self.backbone.layer1(x)
        x = self.backbone.layer2(x)
        # x = self.backbone.layer3(x)
        # x = self.backbone.layer4(x)
        
        pos_embed = self.pos_embed(x)
        x = self.input_project(x)

        x = x.flatten(2).permute(2, 0, 1)
        pos_embed = pos_embed.flatten(2).permute(2, 0, 1)
        
        x = self.transformer(x + pos_embed,   
                             self.query_embed.weight.unsqueeze(1).repeat((1, bs, 1))).permute(1, 0, 2)

        outputs_class = self.class_embed(x)
        outputs_coord = self.box_embed(x).sigmoid()
        out = {'pred_logits': outputs_class, 'pred_boxes': outputs_coord}
        return out

def giou(boxes1, boxes2):
    """
    Params:
        - boxes1 in format (x1, y1, x2, y2) shape: (N,4)
        - boxes2 in format (x1, y1, x2, y2) shape: (M,4)
    """
    area1 = (boxes1[:, 3] - boxes1[:, 1]) * (boxes1[:, 2] - boxes1[:, 0])
    area2 = (boxes2[:, 3] - boxes2[:, 1]) * (boxes2[:, 2] - boxes2[:, 0])

    lt = torch.max(boxes1[:, None, :2], boxes2[:, :2]) # shape: N,M,2
    rb = torch.min(boxes1[:, None, 2:], boxes2[:, 2:]) # shape: N,M,2
    wh = (rb - lt).clamp(min=0.0)
    inter = wh[:, :, 0] * wh[:, :, 1] # N, M
    
    union = area1[:, None] + area2 - inter
    iou = inter / union
    
    lt = torch.min(boxes1[:, None, :2], boxes2[:, :2]) # shape: N,M,2
    rb = torch.max(boxes1[:, None, 2:], boxes2[:, 2:]) # shape: N,M,2
    wh = (rb - lt).clamp(min=0.0)
    area = wh[:, :, 0] * wh[:, :, 1] # N, M

    return iou - (area - union) / area
    
    
def cxcywh2x1y1x2y2(x):
    x_c, y_c, w, h = x.unbind(-1)
    b = [(x_c - 0.5 * w), (y_c - 0.5 * h),
         (x_c + 0.5 * w), (y_c + 0.5 * h)]    
    return torch.stack(b, dim=-1)


class HungarianMatcher(torch.nn.Module):
    def __init__(self, 
                 cost_class,
                 cost_bbox,
                 cost_giou):
        super(HungarianMatcher, self).__init__()
        self.cost_class = cost_class
        self.cost_bbox = cost_bbox
        self.cost_giou = cost_giou
    
    @torch.no_grad()
    def forward(self, outputs, targets):
        bs, num_queries = outputs["pred_logits"].shape[:2]
        out_prob = outputs["pred_logits"].flatten(0, 1).softmax(-1)
        out_bbox = outputs["pred_boxes"].flatten(0, 1)
        # concat target labels and boxes for eyery batch
        tgt_ids = torch.cat([v["labels"] for v in targets])
        tgt_bbox = torch.cat([v["boxes"] for v in targets])
        
        cost_class = -out_prob[:, tgt_ids]
        cost_bbox = torch.cdist(out_bbox, tgt_bbox, p=1)
        cost_giou = -giou(cxcywh2x1y1x2y2(out_bbox), cxcywh2x1y1x2y2(tgt_bbox))
        all_cost = self.cost_class * cost_class + \
                   self.cost_bbox * cost_bbox + \
                self.cost_giou * cost_giou
        all_cost = all_cost.view(bs, num_queries, -1).cpu()
        sizes = [len(v["boxes"]) for v in targets]
        indices = [linear_sum_assignment(c[i]) for i, c in enumerate(all_cost.split(sizes, -1))]
        return [(torch.as_tensor(i, dtype=torch.int64), torch.as_tensor(j, dtype=torch.int64)) for i, j in indices]


class DETRLoss(torch.nn.Module):
    def __init__(self, num_classes, matcher, weight_dict, eos_coef):
        super(DETRLoss, self).__init__()
        self.num_classes = num_classes
        self.matcher = matcher
        self.weight_dict = weight_dict
        self.eos_coef = eos_coef
        empty_weight = torch.ones(self.num_classes + 1)
        empty_weight[-1] = eos_coef
        self.register_buffer("empty_weight", empty_weight)
        self.losses = ['labels', 'boxes']
        
    def loss_labels(self, outputs, targets, indices, num_boxes):
        assert 'pred_logits' in outputs
        pred_logits = outputs["pred_logits"]
        idx = self._get_src_permutation_idx(indices)
        target_classes_output = torch.cat([t["labels"][j] for t, (_, j) in zip(targets, indices)])
        target_classes_all = torch.full(pred_logits.shape[:2], self.num_classes,
                                        dtype=torch.int64, device=pred_logits.device)
        target_classes_all[idx] = target_classes_output
        loss_ce = torch.nn.functional.cross_entropy(pred_logits.transpose(1, 2),
                                                    target_classes_all, 
                                                    self.empty_weight)
        losses = {'loss_ce': loss_ce}
        return losses
    
    def loss_boxes(self, outputs, targets, indices, num_boxes):
        assert 'pred_boxes' in outputs
        idx = self._get_src_permutation_idx(indices)
        src_boxes = outputs["pred_boxes"][idx]
        target_boxes = torch.cat([t["boxes"][i] for t, (_, i) in zip(targets, indices)], dim=0)
        loss_bbox = torch.nn.functional.l1_loss(src_boxes, target_boxes, reduction='none')
        losses = {}
        losses["loss_bbox"] = loss_bbox.sum() / num_boxes
        loss_giou = 1 - torch.diag(giou(cxcywh2x1y1x2y2(src_boxes),
                                   cxcywh2x1y1x2y2(target_boxes)))
        losses['loss_giou'] = loss_giou.sum() / num_boxes
        return losses
        
    def _get_src_permutation_idx(self, indices):
        batch_idx = torch.cat([torch.full_like(src, i) for i, (src, _) in enumerate(indices)])
        src_idx = torch.cat([src for (src, _) in indices])
        return batch_idx, src_idx
    
    def forward(self, outputs, targets):
        outputs_without_aux = {k: v for k, v in outputs.items() if k != 'aux_outputs'}
        indices = self.matcher(outputs_without_aux, targets)

        num_boxes = sum([len(t["labels"]) for t in targets])
        num_boxes = torch.as_tensor([num_boxes],
                                    dtype=torch.float,
                                    device=outputs_without_aux["pred_boxes"].device)
        loss_map = {
            'labels': self.loss_labels,
            'boxes': self.loss_boxes
        }
        
        losses = {}
        for loss in self.losses:
            losses.update(loss_map[loss](outputs_without_aux, targets, indices, num_boxes))
  
        for k,v in losses.items():
            print(f"{k}: {v.item()}")
        return losses

def train():
    num_classes = 1
    batch_size = 12
    epoches = 500
    dataset_train = MiniCOCODetectDataset(mode="train")
    dataset_val = MiniCOCODetectDataset(mode="val")
    device = torch.device("cuda:0") if torch.cuda.is_available() else torch.device("cpu")
    
    writer = SummaryWriter()
    sampler_train = torch.utils.data.RandomSampler(dataset_train)
    sampler_val = torch.utils.data.SequentialSampler(dataset_val)
    batch_sampler_train = torch.utils.data.BatchSampler(
        sampler_train, batch_size, drop_last=True)
    data_loader_train = torch.utils.data.DataLoader(dataset_train, 
                                                    batch_sampler=batch_sampler_train,
                                                    collate_fn=collate_fn, 
                                                    num_workers=2)
    data_loader_val = torch.utils.data.DataLoader(dataset_val, 
                                                  batch_size, 
                                                  sampler=sampler_val,
                                                  drop_last=False, 
                                                  collate_fn=collate_fn, 
                                                  num_workers=2)
    model = DETR(num_queries=10,
                 num_classes=num_classes)
    model.to(device)
    matcher = HungarianMatcher(cost_class=1,
                               cost_bbox=5,
                               cost_giou=2)
    matcher.to(device)
    weight_dict = dict(loss_ce=1,
                       loss_bbox=5,
                       loss_giou=2)
    loss = DETRLoss(num_classes,
                    matcher,
                    weight_dict,
                    eos_coef=0.02)
    loss.to(device)
    n_parameters = sum(p.numel() for p in model.parameters() if p.requires_grad)
    print('number of params:', n_parameters)

    optimizer = torch.optim.AdamW(model.parameters(), \
                                  lr=0.0001,
                                  weight_decay=1e-4)
    lr_scheduler = torch.optim.lr_scheduler.StepLR(optimizer,
                                                   step_size=100, 
                                                   gamma=0.5)
    for i in range(epoches):
        lr_scheduler.step()
        model.train()
        loss.train()
        for j, (images, targets) in enumerate(data_loader_train):
            images = images.to(device)
            for target in targets:
                for k, v in target.items():
                    if isinstance(v, torch.Tensor):
                        target[k] = v.to(device)
            outputs = model(images)
            loss_dict = loss(outputs, targets)
            for k,v in loss_dict.items():
                writer.add_scalar(f"{k}_loss/train", v.item(), i * len(data_loader_train) + j)
            losses = sum(loss_dict[k] * weight_dict[k] for k in loss_dict.keys() if k in weight_dict)
            optimizer.zero_grad()
            losses.backward()
            optimizer.step()
            print(f"[{j}/{i}]===>>>{losses.item()}")
        torch.save(model.state_dict(), "best.pt")
    writer.flush()
    writer.close()

@torch.no_grad()
def detect():
    ckpt = "best.pt"
    num_classes = 1
    ow, oh = 120, 120 
    model = DETR(num_queries=10,
                 num_classes=num_classes)
    
    device = torch.device("cuda:1") if torch.cuda.is_available() else torch.device("cpu")
    model.to(device)
    model.load_state_dict(torch.load(ckpt, map_location=device))
    x = "./datasets/images/train/2.jpg"
    img = cv2.imread(x)
    image = PIL.Image.open(x)
    transforms = torchvision.transforms.Compose([torchvision.transforms.ToTensor()])
    x = transforms(image)[None]
    x = x.to(device)
    with torch.no_grad():
        y = model(x)
        probs = y["pred_logits"].softmax(-1).squeeze(1)
        probs = probs[0, :, :-1]
        keep = probs.max(-1).values > 0.9

        boxes = y['pred_boxes'][0, keep]
        print(f"number of boxes: {len(boxes)}, boxes: {boxes}, probs: {probs[keep]}")
        for box in boxes:
            x1, y1, x2, y2 = box
            x1, y1, x2, y2 = int(x1*ow), int(y1*oh), int(x2*ow), int(y2*oh)
            cv2.rectangle(img, (x1 - x2 // 2, y1 - y2 // 2), (x1+x2//2, y1+y2//2), (255, 125, 245), 3)
    cv2.imwrite("test.png", img)
    
if __name__ == "__main__":
    # generate_mini_coco_detect_dataset()
    import sys
    if sys.argv[1] == "train":
        train()
    else:
        detect()
    # x = torch.randn(1, 3, 224, 224)
    # detr = DETR()
    # y = detr(x)
    # boxes1 = torch.Tensor([[0, 0, 2, 2]])
    # boxes2 = torch.Tensor([[1, 1, 3, 3]])
    # g_iou = giou(boxes1, boxes2)
    # print(g_iou)
    # outputs = {
    #     "pred_logits": torch.Tensor([[[0.1, 0.9, 0.0], [0.2, 0.8, 0.0]]]),
    #     "pred_boxes": torch.Tensor([[[2, 2, 2, 2], [0, 0, 2, 2]]])
    # }
    # targets = [{"labels": torch.as_tensor([1, 1], dtype=torch.long),
    #             "boxes": torch.Tensor([[1, 1, 2, 2],
    #                                    [3, 3, 2, 2]])}]
    # matcher = HungarianMatcher(1, 2, 1)
    # # print(matcher(outputs, targets))
    # weight_dict = dict(loss_ce=1,
    #                    loss_bbox=5,
    #                    loss_giou=2)
    
    # criterion = DETRLoss(2, matcher, weight_dict, eos_coef=0.1)
    # losses = criterion(outputs, targets)
    # print(losses)
    