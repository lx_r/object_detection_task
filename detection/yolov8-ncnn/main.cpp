#include <cstdio>

#include <yolov8.h>

int main(int argc, char **argv)
{
    if(argc < 4) {
        fprintf(stderr, "Usage: ./main [param_file] [bin_file] [test_image]\n");
        return -1;
    }
    auto detector = cvx::Yolo8(argv[1], argv[2]);
    cv::Mat image = cv::imread(argv[3]);
    cv::cvtColor(image, image, cv::COLOR_BGR2RGB);
    std::vector<cvx::Instance> insts;
    detector.inference(image, insts);
    return 0;
}