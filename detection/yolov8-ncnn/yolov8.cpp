#include <cassert>
#include <cfloat>

#include <benchmark.h>
#include <yolov8.h>
#include <glog/logging.h>


namespace cvx
{

    Yolo8::Yolo8(const char* param_file, const char* bin_file)
    {
        net_ = std::make_unique<ncnn::Net>();
        // net_->opt.use_vulkan_compute = true;
        assert(net_->load_param(param_file) == 0);
        assert(net_->load_model(bin_file) == 0);
        classes_ = {"device"};
        kpt_shape_[0] = 17;
        kpt_shape_[1] = 3;
        LOG(INFO) << "Yolo8 loaded";
    }

    Yolo8::~Yolo8()
    {

    }

    void Yolo8::visualize(const cv::Mat &image, 
                          const std::vector<Instance> &instances, 
                          int top_pad,
                          int left_pad,
                          float scale)
    {
        size_t w = image.cols;
        size_t h = image.rows;
        cv::Mat visual;
        cv::resize(image, visual, cv::Size(w * scale, h * scale));
        cv::copyMakeBorder(visual, visual, top_pad, top_pad, left_pad, left_pad, cv::BORDER_CONSTANT, cv::Scalar(0, 125, 0));
        std::cout << "visual.size: " << visual.size() << std::endl;
        for(auto &inst : instances) {
            cv::rectangle(visual, inst.box, cv::Scalar(0, 255, 0), 1, 8, 0);
            for(auto &kpt : inst.keypoints) {
                cv::circle(visual, cv::Point(kpt.x, kpt.y), 2, cv::Scalar(0, 255, 0), 2);
            }
        }
        cv::cvtColor(visual, visual, cv::COLOR_RGB2BGR);
        cv::imwrite("visual.png", visual);
    }

    void Yolo8::decodeInstances(ncnn::Mat &data, std::vector<Instance> &instances) const
    {
        std::vector<int> class_ids;
        std::vector<float> confidences;
        std::vector<cv::Rect> boxes;
        std::vector<std::vector<cvx::KeyPoint>> kpList;
        float *data_ptr = static_cast<float *>(data.data);
        float resizeScales  = 1.0;
        int class_id;
        
        for(size_t i = 0; i < data.w; i ++) {
            float *data_col = data_ptr + i;
            double maxClassScore = DBL_MIN;
            for(size_t j = 0; j < classes_.size(); ++j) {
                if(*(data_col + (4 + j) * data.w) > maxClassScore) {
                    maxClassScore = *(data_col + (4 + j) * data.w);
                    class_id = j;
                }
            }
            if (maxClassScore > score_threshold_)
            {
                confidences.push_back(maxClassScore);
                class_ids.push_back(class_id);
                float x = *(data_col);
                float y = *(data_col + data.w);
                float w = *(data_col + 2 * data.w);
                float h = *(data_col + 3 * data.w);
                int left = int((x - 0.5 * w) * resizeScales);
                int top = int((y - 0.5 * h) * resizeScales);

                int width = int(w * resizeScales);
                int height = int(h * resizeScales);

                boxes.push_back(cv::Rect(left, top, width, height));
                
                auto kp_ptr = data_col + 4  * data.w;
                std::vector<cvx::KeyPoint> kps;
                float kps_x, kps_y, kps_v;
                for (int k = 0; k < kpt_shape_[0]; k++) {
                    if(kpt_shape_[1] ==3) {
                        kps_v = *(kp_ptr + (kpt_shape_[1] * k) * data.w);
                        kps_x = *(kp_ptr + (kpt_shape_[1] * k + 1) * data.w);
                        kps_y = *(kp_ptr + (kpt_shape_[1] * k + 2) * data.w);
                    } else {
                        kps_x = *(kp_ptr + (kpt_shape_[1] * k) * data.w);
                        kps_y = *(kp_ptr + (kpt_shape_[1] * k + 1) * data.w);
                        kps_v = 1.0f;
                    }
                    kps.emplace_back(kps_x * resizeScales, kps_y * resizeScales, kps_v, false);
                }
                kpList.push_back(kps);
            }
        }

        std::vector<int> nmsResult;
        cv::dnn::NMSBoxes(boxes, confidences, score_threshold_, iou_threshold_, nmsResult);
        for (int i = 0; i < nmsResult.size(); ++i)
        {
            int idx = nmsResult[i];
            instances.emplace_back(boxes[idx],
                                   class_ids[idx],
                                   confidences[idx],
                                   cv::Mat(),
                                   kpList[idx]);
        }

    }

    int Yolo8::inference(const cv::Mat &image, std::vector<Instance> &instances) const
    {
        int target_size = 640;
        int img_w = image.cols;
        int img_h = image.rows;

        // letterbox pad to multiple of MAX_STRIDE
        int w = img_w;
        int h = img_h;
        float scale = 1.f;
        if (w > h)
        {
            scale = (float)target_size / w;
            w = target_size;
            h = h * scale;
        }
        else
        {
            scale = (float)target_size / h;
            h = target_size;
            w = w * scale;
        }

        ncnn::Mat in = ncnn::Mat::from_pixels_resize(image.data, ncnn::Mat::PIXEL_BGR2RGB, img_w, img_h, w, h);

        int wpad = target_size - w;
        int hpad = target_size - h;

        int top = hpad / 2;
        int bottom = hpad - top;
        int left = wpad / 2;
        int right = wpad - left;

        ncnn::Mat in_pad;
        ncnn::copy_make_border(in,
            in_pad,
            top,
            bottom,
            left,
            right,
            ncnn::BORDER_CONSTANT,
            114.f);

        const float norm_vals[3] = { 1 / 255.f, 1 / 255.f, 1 / 255.f };
        in_pad.substract_mean_normalize(0, norm_vals);

        auto t0 = ncnn::get_current_time();
        ncnn::Extractor ex = net_->create_extractor();
        ex.input("in0", in_pad);
        ncnn::Mat out;
        ex.extract("out0", out);
        this->decodeInstances(out, instances);
        auto t1 = ncnn::get_current_time();
        LOG(INFO) << "Perf: " << (t1 - t0) << "ms" << std::endl;
        this->visualize(image, instances, top, left, scale);
        LOG(INFO) << "instances.size: " << instances.size() << std::endl;

        return 0;
    }
}